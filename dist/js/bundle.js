/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./helpers */ \"./src/js/helpers.js\");\n\nvar main = document.querySelector('main');\nvar id = main.getAttribute('id');\n\nswitch (id) {\n  case 'login-page':\n    __webpack_require__(/*! ./pages/login */ \"./src/js/pages/login.js\");\n\n    break;\n\n  case 'fleet-create-page':\n    __webpack_require__(/*! ./pages/fleet-create */ \"./src/js/pages/fleet-create.js\");\n\n    break;\n\n  case 'projects-create-page':\n    __webpack_require__(/*! ./pages/fleet-create */ \"./src/js/pages/fleet-create.js\");\n\n    break;\n\n  case 'news-create-page':\n    __webpack_require__(/*! ./pages/news-create */ \"./src/js/pages/news-create.js\");\n\n    break;\n}\n\n//# sourceURL=webpack:///./src/js/app.js?");

/***/ }),

/***/ "./src/js/helpers.js":
/*!***************************!*\
  !*** ./src/js/helpers.js ***!
  \***************************/
/*! exports provided: handleAddImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"handleAddImage\", function() { return handleAddImage; });\nvar deleteImage = function deleteImage() {\n  $('.delete').on('click', function () {\n    var photoWrap = $(this).parents('.inputs__photo-preview');\n    photoWrap.parent().remove();\n  });\n};\n\nvar viewImage = function viewImage() {\n  $('.view').on('click', function () {\n    var imgSrc = $(this).attr('data-src');\n    var photoWrap = $('.modal-photo-image');\n    photoWrap.html('');\n    photoWrap.append(\"<img src=\\\"\".concat(imgSrc, \"\\\" alt=\\\"Modal photo\\\"/>\"));\n  });\n};\n\nvar editImage = function editImage() {\n  $('.edit').on('change', function (e) {\n    var input = e.target;\n    var submitBtn = $('button[type=submit]');\n    var image = $(this).parents('.inputs__photo-preview').find('.inputs__photo img');\n\n    if (submitBtn.hasClass('d-none')) {\n      submitBtn.removeClass('d-none');\n    }\n\n    if (input.files[0]) {\n      var reader = new FileReader();\n\n      reader.onload = function (e) {\n        var src = e.target.result;\n        image.attr('src', src);\n      };\n\n      reader.readAsDataURL(input.files[0]);\n    }\n  });\n};\n\nvar handleAddImage = function handleAddImage(e) {\n  var input = e.target;\n  var submitBtn = $('button[type=submit]');\n  var size = $(input).attr('data-img-size');\n  var type = $(input).attr('data-img-type');\n  var photosRow = type === 'news' ? $(input).parents('.photos-wrap') : $('.photos-wrap');\n\n  if (submitBtn.hasClass('d-none')) {\n    submitBtn.removeClass('d-none');\n  } // photosRow.html('')\n\n\n  Object.values(input.files).map(function (file, index) {\n    var reader = new FileReader();\n\n    reader.onload = function (e) {\n      var src = e.target.result;\n      var photoTemplate = \"\\n        <div class=\\\"\".concat(type === 'certificate' || type === 'news' ? 'col-3 mb-4' : type === 'partners' ? 'col-3 mb-5' : size === 'big' ? 'col-4 mb-4' : 'col-2 mb-4', \"\\\">\\n          <div class=\\\"inputs__photo-preview mb-3 \").concat(size === 'big' ? 'inputs__photo-preview_big' : 'inputs__photo-preview_small', \"\\\">\\n            <div class=\\\"inputs__photo-actions\\\">\\n              \").concat(size === 'small' ? \"\\n                <button class=\\\"action-btn view\\\" data-toggle=\\\"modal\\\" data-target=\\\"#photo-modal\\\" data-src=\\\"\".concat(src, \"\\\">\\n                   <img class=\\\"icon-action\\\" src=\\\"img/icons/ViewIcon.svg\\\" alt=\\\"View\\\">\\n                </button>\\n              \") : '', \"\\n              <div class=\\\"form-group action-btn edit\\\">\\n                <label for=\\\"edit-photo-\").concat(index + 1, \"\\\">\\n                  <img class=\\\"icon-action\\\" src=\\\"img/icons/EditIcon.svg\\\" alt=\\\"Edit\\\">\\n                  \").concat(size === 'big' ? \"<span>\\u0420\\u0435\\u0434\\u0430\\u043A\\u0442\\u0438\\u0440\\u043E\\u0432\\u0430\\u0442\\u044C</span>\" : '', \"\\n                </label>\\n                <input type=\\\"file\\\" accept=\\\"image/*\\\" id=\\\"edit-photo-\").concat(index + 1, \"\\\" class=\\\"edit\\\">\\n              </div>\\n              <button class=\\\"action-btn delete\\\">\\n                 <img class=\\\"icon-action\\\" src=\\\"img/icons/DeleteIcon.svg\\\" alt=\\\"Delete\\\">\\n                 \").concat(size === 'big' ? \"<span>\\u0423\\u0434\\u0430\\u043B\\u0438\\u0442\\u044C</span>\" : '', \"\\n              </button>\\n            </div>\\n            <div class=\\\"inputs__photo\\\">\\n              <img src=\\\"\").concat(src, \"\\\" alt=\\\"\").concat(file.name, \"\\\">\\n            </div>\\n          </div> \\n          \").concat(type === 'certificate' ? \"\\n            <div class=\\\"form-group\\\">\\n              <input type=\\\"text\\\" class=\\\"form-control\\\" name=\\\"certificate-name-\".concat(index + 1, \"\\\" placeholder=\\\"\\u041D\\u0430\\u0437\\u0432\\u0430\\u043D\\u0438\\u0435 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043A\\u0430\\u0442\\u0430\\\">          \\n            </div>\\n          \") : '', \"      \\n        </div>\\n      \");\n      type === 'news' ? photosRow.prepend(photoTemplate) : photosRow.append(photoTemplate);\n      viewImage();\n      editImage();\n      deleteImage();\n    };\n\n    reader.readAsDataURL(file);\n  });\n};\n\n$('.header__collapse').on('click', function () {\n  $('.header').toggleClass('is-collapsed');\n});\n$('.main-form').on('submit', function (e) {\n  e.preventDefault();\n  $(this).find('input').map(function (index, input) {\n    console.log({\n      name: $(input).attr('name'),\n      value: $(input).val()\n    });\n  });\n});\n$('.dropdown-item.delete').on('click', function () {\n  var tBody = $(this).parents('tbody');\n  var deleteRowTemplate = \"\\n    <tr>\\n      <td colspan=\\\"5\\\">\\n        <div id=\\\"form-delete\\\">\\n          <form action=\\\"/\\\">\\n            <p class=\\\"delete-text\\\">\\u0412\\u044B \\u0443\\u0432\\u0435\\u0440\\u0435\\u043D\\u044B \\u0447\\u0442\\u043E \\u0445\\u043E\\u0442\\u0438\\u0442\\u0435 \\u0443\\u0434\\u0430\\u043B\\u0438\\u0442\\u044C \\xAB\\u042D\\u0442\\u043E \\u043D\\u0430\\u0448 \\u043B\\u0443\\u0447\\u0448\\u0438\\u0439 \\u043F\\u0440\\u043E\\u0435\\u043A\\u0442 \\u0437\\u0430 \\u043D\\u0435\\u0434\\u0435\\u043B\\u044E\\xBB?</p>\\n            <a href=\\\"#\\\" class=\\\"delete-btn\\\">\\u0414\\u0430</a>\\n            <a href=\\\"javascript:void[0]\\\" class=\\\"cancel-btn\\\">\\u041E\\u0442\\u043C\\u0435\\u043D\\u0430</a>\\n          </form>\\n        </div>\\n      </td>    \\n    </tr>\\n  \";\n  tBody.prepend(deleteRowTemplate);\n  $('.cancel-btn').on('click', function () {\n    $(this).parents('tr').remove();\n  });\n});\n$('.form-control-file').on('change', handleAddImage);\nviewImage();\neditImage();\ndeleteImage();\n\n\n//# sourceURL=webpack:///./src/js/helpers.js?");

/***/ }),

/***/ "./src/js/pages/fleet-create.js":
/*!**************************************!*\
  !*** ./src/js/pages/fleet-create.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$('.btn-add-blocks').on('click', function () {\n  var blockRow = $(this).parents('.inputs').find('.row');\n  var blockCols = blockRow.find('.col-4');\n  blockRow.append(\"\\n    <div class=\\\"col-4 mb-4\\\">\\n      <div class=\\\"form-group input-text vertical\\\">\\n        <input type=\\\"text\\\" name=\\\"description-title-\".concat(blockCols.length + 1, \"\\\" placeholder=\\\"\\u0417\\u0430\\u0433\\u043E\\u043B\\u043E\\u0432\\u043E\\u043A\\\" class=\\\"form-control\\\"/>\\n      </div>\\n      <div class=\\\"form-group input-text vertical text-area\\\">\\n        <textarea name=\\\"description-\").concat(blockCols.length + 2, \"\\\" rows=\\\"6\\\" placeholder=\\\"\\u041F\\u043E\\u0434\\u0437\\u0430\\u0433\\u043E\\u043B\\u043E\\u0432\\u043E\\u043A\\\" class=\\\"form-control\\\"></textarea>\\n      </div>\\n    </div>\\n  \"));\n});\n\n//# sourceURL=webpack:///./src/js/pages/fleet-create.js?");

/***/ }),

/***/ "./src/js/pages/login.js":
/*!*******************************!*\
  !*** ./src/js/pages/login.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$('form').on('submit', function (e) {\n  e.preventDefault();\n  window.location.href = \"\".concat(window.location.origin, \"/fleet.html\");\n});\n\n//# sourceURL=webpack:///./src/js/pages/login.js?");

/***/ }),

/***/ "./src/js/pages/news-create.js":
/*!*************************************!*\
  !*** ./src/js/pages/news-create.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers */ \"./src/js/helpers.js\");\n\n\nvar blockTemplate = function blockTemplate(index) {\n  return \"\\n  <div class=\\\"row mb-5 news-row\\\">\\n    <div class=\\\"col-12\\\">\\n      <div class=\\\"row photos-wrap\\\">\\n        <div class=\\\"col-3 mb-4\\\">\\n          <div class=\\\"form-group add-file\\\">\\n            <label for=\\\"news-photos-\".concat(index, \"\\\" class=\\\"add-icon\\\">\\n              <span></span>\\n              <span></span>\\n            </label>\\n            <input class=\\\"form-control-file\\\" type=\\\"file\\\" accept=\\\"image/*\\\" multiple \\n                  id=\\\"news-photos-\").concat(index, \"\\\" data-img-size=\\\"big\\\" data-img-type=\\\"news\\\">\\n          </div>\\n        </div>\\n      </div>\\n    </div>\\n    <div class=\\\"col-12\\\">\\n      <div class=\\\"form-group text-area\\\">\\n        <textarea class=\\\"form-control\\\" name=\\\"news-text-\").concat(index, \"\\\" rows=\\\"6\\\" placeholder=\\\"\\u0422\\u0435\\u043A\\u0441\\u0442 \\u043D\\u043E\\u0432\\u043E\\u0441\\u0442\\u0438\\\"></textarea>\\n      </div>\\n    </div>    \\n  </div>\\n\");\n};\n\n$('.add-news-block').on('click', function () {\n  var rowInputs = $(this).parents('.inputs');\n  var rowCount = rowInputs.find('.news-row').length;\n  rowInputs.append(blockTemplate(rowCount));\n  $('.form-control-file').on('change', _helpers__WEBPACK_IMPORTED_MODULE_0__[\"handleAddImage\"]);\n});\n\n//# sourceURL=webpack:///./src/js/pages/news-create.js?");

/***/ }),

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/scss/style.scss?");

/***/ }),

/***/ 0:
/*!***************************************************!*\
  !*** multi ./src/js/app.js ./src/scss/style.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./src/js/app.js */\"./src/js/app.js\");\nmodule.exports = __webpack_require__(/*! ./src/scss/style.scss */\"./src/scss/style.scss\");\n\n\n//# sourceURL=webpack:///multi_./src/js/app.js_./src/scss/style.scss?");

/***/ })

/******/ });