require('./helpers')

const main = document.querySelector('main')
const id = main.getAttribute('id')

switch (id) {
  case 'login-page':
    require('./pages/login')
    break
  case 'fleet-create-page':
    require('./pages/fleet-create')
    break
  case 'projects-create-page':
    require('./pages/fleet-create')
    break
  case 'news-create-page':
    require('./pages/news-create')
    break
}
