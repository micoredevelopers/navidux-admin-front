const deleteImage = () => {
  $('.delete').on('click', function () {
    const photoWrap = $(this).parents('.inputs__photo-preview')

    photoWrap.parent().remove()
  })
}

const viewImage = () => {
  $('.view').on('click', function () {
    const imgSrc = $(this).attr('data-src')
    const photoWrap = $('.modal-photo-image')

    photoWrap.html('')
    photoWrap.append(`<img src="${imgSrc}" alt="Modal photo"/>`)
  })
}

const editImage = () => {
  $('.edit').on('change', function (e) {
    const input = e.target
    const submitBtn = $('button[type=submit]')
    const image = $(this).parents('.inputs__photo-preview').find('.inputs__photo img')

    if (submitBtn.hasClass('d-none')) {
      submitBtn.removeClass('d-none')
    }

    if (input.files[0]) {
      const reader = new FileReader()

      reader.onload = function (e) {
        const src = e.target.result

        image.attr('src', src)
      }

      reader.readAsDataURL(input.files[0])
    }
  })
}

const handleAddImage = (e) => {
  const input = e.target
  const submitBtn = $('button[type=submit]')
  const size = $(input).attr('data-img-size')
  const type = $(input).attr('data-img-type')
  const photosRow = type === 'news' ? $(input).parents('.photos-wrap') : $('.photos-wrap')

  if (submitBtn.hasClass('d-none')) {
    submitBtn.removeClass('d-none')
  }

  // photosRow.html('')

  Object.values(input.files).map((file, index) => {
    const reader = new FileReader()

    reader.onload = function (e) {
      const src = e.target.result
      const photoTemplate = `
        <div class="${(type === 'certificate' || type === 'news') 
                      ? 'col-3 mb-4' : type === 'partners' 
                      ? 'col-3 mb-5' : size === 'big' 
                      ? 'col-4 mb-4' : 'col-2 mb-4'}">
          <div class="inputs__photo-preview mb-3 ${size === 'big' ? 'inputs__photo-preview_big' : 'inputs__photo-preview_small'}">
            <div class="inputs__photo-actions">
              ${size === 'small' ? `
                <button class="action-btn view" data-toggle="modal" data-target="#photo-modal" data-src="${src}">
                   <img class="icon-action" src="img/icons/ViewIcon.svg" alt="View">
                </button>
              ` : ''}
              <div class="form-group action-btn edit">
                <label for="edit-photo-${index + 1}">
                  <img class="icon-action" src="img/icons/EditIcon.svg" alt="Edit">
                  ${size === 'big' ? `<span>Редактировать</span>` : ''}
                </label>
                <input type="file" accept="image/*" id="edit-photo-${index + 1}" class="edit">
              </div>
              <button class="action-btn delete">
                 <img class="icon-action" src="img/icons/DeleteIcon.svg" alt="Delete">
                 ${size === 'big' ? `<span>Удалить</span>` : ''}
              </button>
            </div>
            <div class="inputs__photo">
              <img src="${src}" alt="${file.name}">
            </div>
          </div> 
          ${type === 'certificate' ? `
            <div class="form-group">
              <input type="text" class="form-control" name="certificate-name-${index + 1}" placeholder="Название сертификата">          
            </div>
          ` : ''}      
        </div>
      `

      type === 'news'
        ? photosRow.prepend(photoTemplate)
        : photosRow.append(photoTemplate)

      viewImage()
      editImage()
      deleteImage()
    }

    reader.readAsDataURL(file)
  })
}

$('.header__collapse').on('click', function () {
  $('.header').toggleClass('is-collapsed')
})

$('.main-form').on('submit', function (e) {
  e.preventDefault()

  $(this).find('input').map((index, input) => {
    console.log({ name: $(input).attr('name'), value: $(input).val() })
  })
})

$('.dropdown-item.delete').on('click', function () {
  const tBody = $(this).parents('tbody')
  const deleteRowTemplate = `
    <tr>
      <td colspan="5">
        <div id="form-delete">
          <form action="/">
            <p class="delete-text">Вы уверены что хотите удалить «Это наш лучший проект за неделю»?</p>
            <a href="#" class="delete-btn">Да</a>
            <a href="javascript:void[0]" class="cancel-btn">Отмена</a>
          </form>
        </div>
      </td>    
    </tr>
  `

  tBody.prepend(deleteRowTemplate)

  $('.cancel-btn').on('click', function () {
    $(this).parents('tr').remove()
  })
})

$('.form-control-file').on('change', handleAddImage)

viewImage()
editImage()
deleteImage()

export { handleAddImage }
