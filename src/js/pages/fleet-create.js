$('.btn-add-blocks').on('click', function () {
  const blockRow = $(this).parents('.inputs').find('.row')
  const blockCols = blockRow.find('.col-4')

  blockRow.append(`
    <div class="col-4 mb-4">
      <div class="form-group input-text vertical">
        <input type="text" name="description-title-${blockCols.length + 1}" placeholder="Заголовок" class="form-control"/>
      </div>
      <div class="form-group input-text vertical text-area">
        <textarea name="description-${blockCols.length + 2}" rows="6" placeholder="Подзаголовок" class="form-control"></textarea>
      </div>
    </div>
  `)
})
